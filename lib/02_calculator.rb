def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(sum_array)
  idx = 0
  sum = 0

  while idx < sum_array.length
    sum += sum_array[idx]
    idx += 1
  end

  sum
end

def multiply(multiply_arr)
  idx = 0
  product = 1 unless multiply_arr.empty?

  while idx < multiply_arr.length
    product *= multiply_arr[idx]
    idx += 1
  end

  product
end

def power(num, power)
  num ** power
end

def factorial(num)
  current_factorial = num
  factorial = 1

  while current_factorial > 0
    factorial *= current_factorial
    current_factorial -= 1
  end

  factorial
end
