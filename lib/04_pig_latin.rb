def translate(string)

  def pig_latinize(word)
    #main method to make words into pig latin
    def is_vowel?(letter)
      return true if "aeiou".split("").include?(letter)
      false
    end

    def not_qu?(letter1, letter2)
      return false if "#{letter1}#{letter2}" == "qu"
      true
    end
    letter_arr = word.split("")
    pulled_letters = Array.new

    until is_vowel?(letter_arr[0]) && not_qu?(pulled_letters[-1], letter_arr[0])
      pulled_letters << letter_arr.shift
    end

    pulled_letters << "ay"
    "#{letter_arr.join("")}#{pulled_letters.join("")}"

  end
  word_arr = string.split(" ")
  pig_latin_str = String.new

  word_arr.each do |word|
    pig_latin_str << "#{pig_latinize(word)} "
  end
  pig_latin_str.strip
end
