def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, string_repeat = 2)
  repeat_str = string.dup

  while string_repeat > 1
    repeat_str << " #{string}"
    string_repeat -= 1
  end

  repeat_str
end

def start_of_word(string, num_letters = 1)
  first_letters = String.new
  idx = 0

  while idx < num_letters
    first_letters << string[idx]
    idx += 1
  end

  first_letters
end

def first_word(string)
  word = String.new
  idx = 0

  until string[idx] == " "
    word << string[idx]
    idx += 1
  end

  word
end

def titleize(title)

  def little_word?(string)
    string == "and" || string == "the" || string == "over" ? true : false
  end

  title_words = title.split(" ")
  new_title = title_words.shift.capitalize


  title_words.each do |word|
    little_word?(word) == true ? new_title << " #{word}" : new_title << " #{word.capitalize}"
  end

  new_title
end
