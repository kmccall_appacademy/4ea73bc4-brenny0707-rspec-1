def ftoc(f)
  c = (f.to_f - 32.0) / 9.0 * 5.0
end

def ctof(c)
  f = c.to_f / 5.0 * 9.0 + 32.0
end
